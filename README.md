# Philanthropy Partners

A website to explain what we are all about.

https://matthewwilliamson93.gitlab.io/philanthropy-partners/

## Developing

#### Development Run

To run the code for local development:

```
make run
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

#### Production Run

If you are looking to run the the code for release:

```
make prod-run
```

This will run the app from a local NGINX docker image.

## CI / CD

The repo uses a local gitlab CI config to build the code and deploy it to gitlab pages.

## Repo Layout

```
.
├── craco.config.js
├── deployment
│   ├── app.conf                 - Configuration for the NGINX container
│   └── Dockerfile               - NGINX container that runs the optimized build from the react app
├── Makefile                     - A proxy for actions that can be run in the repo
├── package.json                 - File to hold all the apps settings
├── public
│   ├── index.html               - The entry point for the site
│   ├── manifest.json            - The manifest files is used for PWAs
│   └── robots.txt
├── README.md
├── src
│   ├── About.tsx                - Contains a basic about page for the site
│   ├── App.tsx                  - Contains all the framework for the site
│   ├── Header.tsx               - Creates the header for the site
│   ├── Popup.tsx                - Contains a popup for when looking through the workflow
│   ├── data.ts                  - Contains most of the raw data for the workflow
│   ├── index.tsx                - Starts the react app
│   ├── react-app-env.d.ts
│   ├── registerServiceWorker.ts - Handles setting up the service worker
│   └── Workflow.tsx             - Creates flow diagram for people to understand what its all about
├── tsconfig.json                - The configuration for typescript
└── yarn.lock                    - The specific dependencies tested
```
