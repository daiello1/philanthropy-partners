import MenuIcon from "@mui/icons-material/Menu";

import AppBar from "@mui/material/AppBar";
import Button from "@mui/material/Button";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import Link from "@mui/material/Link";
import MenuItem from "@mui/material/MenuItem";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

import { makeStyles } from "@mui/styles";

import React, { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";

const headersData = [
    {
        label: "About",
        href: "/about",
    },
    {
        label: "Partners",
        href: "/partners",
    },
    {
        label: "Projects",
        href: "/projects",
    },
    {
        label: "Get Involved",
        href: "/get-involved",
    },
];

const useStyles = makeStyles(() => ({
    header: {
        backgroundColor: "#000000",
        paddingRight: "79px",
        paddingLeft: "118px",
        "@media (max-width: 900px)": {
            paddingLeft: 0,
        },
    },
    logo: {
        fontFamily: "Work Sans, sans-serif",
        fontWeight: 600,
        color: "#FFFFFF",
        textAlign: "left",
    },
    menuButton: {
        fontFamily: "Open Sans, sans-serif",
        fontWeight: 700,
        size: "18px",
        marginLeft: "38px",
    },
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
    },
    drawerContainer: {
        padding: "20px 30px",
    },
}));

/*
 * A React component that is meant to display the header for the site
 * and links to routes.
 *
 * https://mui.com/components/app-bar/
 * https://mui.com/components/buttons/
 * https://mui.com/components/drawers/
 * https://mui.com/api/icon-button/
 * https://mui.com/components/links/
 * https://mui.com/api/menu-item/
 * https://mui.com/api/toolbar/
 * https://mui.com/components/typography/
 */
export default function Header(): JSX.Element {
    const { header, logo, menuButton, toolbar, drawerContainer } = useStyles();

    const [state, setState] = useState({
        mobileView: false,
        drawerOpen: false,
    });

    const { mobileView, drawerOpen } = state;

    useEffect(() => {
        const setResponsiveness = () => {
            return window.innerWidth < 900
                ? setState((prevState) => ({ ...prevState, mobileView: true }))
                : setState((prevState) => ({
                      ...prevState,
                      mobileView: false,
                  }));
        };

        setResponsiveness();

        window.addEventListener("resize", () => setResponsiveness());

        return () => {
            window.removeEventListener("resize", () => setResponsiveness());
        };
    }, []);

    const displayDesktop = () => {
        return (
            <Toolbar className={toolbar}>
                {mainLogo}
                <div>{getMenuButtons()}</div>
            </Toolbar>
        );
    };

    const displayMobile = () => {
        const handleDrawerOpen = () =>
            setState((prevState) => ({ ...prevState, drawerOpen: true }));
        const handleDrawerClose = () =>
            setState((prevState) => ({ ...prevState, drawerOpen: false }));

        return (
            <Toolbar>
                <IconButton
                    {...{
                        edge: "start",
                        color: "inherit",
                        "aria-label": "menu",
                        "aria-haspopup": "true",
                        onClick: handleDrawerOpen,
                    }}
                >
                    <MenuIcon />
                </IconButton>

                <Drawer
                    {...{
                        anchor: "left",
                        open: drawerOpen,
                        onClose: handleDrawerClose,
                    }}
                >
                    <div className={drawerContainer}>{getDrawerChoices()}</div>
                </Drawer>

                <div>{mainLogo}</div>
            </Toolbar>
        );
    };

    const getDrawerChoices = () => {
        return headersData.map(({ label, href }) => {
            return (
                <Link
                    color="inherit"
                    component={RouterLink}
                    key={label}
                    style={{ textDecoration: "none" }}
                    to={href}
                >
                    <MenuItem>{label}</MenuItem>
                </Link>
            );
        });
    };

    const mainLogo = (
        <Button
            className={menuButton}
            color="inherit"
            component={RouterLink}
            key="main-logo"
            to="/"
        >
            <Typography variant="h6" component="h1" className={logo}>
                Philanthropy Partners
            </Typography>
        </Button>
    );

    const getMenuButtons = () => {
        return headersData.map(({ label, href }) => {
            return (
                <Button
                    className={menuButton}
                    color="inherit"
                    component={RouterLink}
                    key={label}
                    to={href}
                >
                    {label}
                </Button>
            );
        });
    };

    return (
        <header>
            <AppBar className={header}>
                {mobileView ? displayMobile() : displayDesktop()}
            </AppBar>
        </header>
    );
}
