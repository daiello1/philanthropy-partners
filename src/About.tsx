import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";

import React from "react";

import { NODES } from "./data";

const useStyles = makeStyles(() => ({
    about: {
        "padding-top": "60px",
    },
}));

/*
 * A React component for a simple About page.
 */
export default function About(): JSX.Element {
    const { about } = useStyles();

    return (
        <div className={about}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
                What are we all about?
            </Typography>
            <Typography id="modal-modal-intro">
                Philanthropy Partners (PP) aim to create a pipeline of
                technology skills-based volunteer projects for Bloomberg
                Engineering to implement with BOB hours. Leveraging Bloomberg
                Philanthropy and their nonprofit partners, PP will first
                identify an appropriate project, then confirm a specification,
                design a solution, and organize an implementation into
                actionable, well-defined tasks for engineering to execute.
            </Typography>
            {NODES.filter((node) =>
                node.data.tabs.some((tab) => tab.tag === "pp")
            ).map((node) => (
                <div key={node.data.label}>
                    <Typography
                        id={`${node.data.label}-title`}
                        key={`${node.data.label}-title`}
                        variant="h6"
                        component="h4"
                    >
                        {node.data.label}
                    </Typography>
                    <ul
                        id={`${node.data.label}-data`}
                        key={`${node.data.label}-data`}
                    >
                        {node.data.tabs
                            .filter((tab) => tab.tag === "pp")[0]
                            .description.map((note, index) => (
                                <li
                                    id={`${node.data.label}-data-${index}`}
                                    key={`${node.data.label}-data-${index}`}
                                >
                                    {note}
                                </li>
                            ))}
                    </ul>
                </div>
            ))}
        </div>
    );
}
