import { makeStyles } from "@mui/styles";

import React, { useState } from "react";
import ReactFlow from "react-flow-renderer";

import Popup, { PopupData } from "./Popup";
import { LEGEND_DATA, NODES, EDGES } from "./data";

const useStyles = makeStyles(() => ({
    legendList: {
        margin: 0,
        "margin-bottom": "5px",
        padding: 0,
        float: "left",
        "list-style": "none",
    },
    legendKeys: {
        "font-size": "80%",
        "list-style": "none",
        "margin-left": 0,
        "line-height": "18px",
        "margin-bottom": "2px",
    },
    legendColor: {
        display: "block",
        float: "left",
        height: "16px",
        width: "30px",
        "margin-right": "5px",
        "margin-left": 0,
        border: "1px solid #999",
    },
}));

/*
 * A React component that creates the flow diagram.
 *
 * https://github.com/wbkd/react-flow
 */
export default function Workflow(): JSX.Element {
    const { legendList, legendKeys, legendColor } = useStyles();

    const [open, setOpen] = useState(false);
    const [data, setData] = useState<PopupData | null>(null);

    const onElementClick = (evt, id) => {
        evt.stopPropagation();
        if (id.data.tabs === undefined) {
            return;
        }

        setData({
            label: id.data.label,
            tabs: id.data.tabs.map((tab) => ({
                tag: LEGEND_DATA[tab.tag].tag,
                name: LEGEND_DATA[tab.tag].name,
                description: tab.description,
            })),
        });
        setOpen(true);
    };

    const legendNode = {
        id: "0",
        data: {
            label: (
                <div>
                    <h3>Legend</h3>
                    <ul className={legendList}>
                        {Object.values(LEGEND_DATA).map((legendKey) => (
                            <li className={legendKeys} key={legendKey.tag}>
                                <span
                                    className={legendColor}
                                    style={{ background: legendKey.color }}
                                ></span>
                                {`${legendKey.tag} - ${legendKey.name}`}
                            </li>
                        ))}
                    </ul>
                </div>
            ),
        },
        position: { x: 700, y: 100 },
    };
    const nodes = NODES.map((node) => ({
        ...node,
        draggable: false,
        connectable: false,
        style: { background: LEGEND_DATA[node.data.tabs[0].tag].color },
    }));

    const elements = [legendNode, ...nodes, ...EDGES];

    return (
        <div>
            <ReactFlow
                elements={elements}
                onElementClick={onElementClick}
                snapToGrid={true}
                snapGrid={[15, 15]}
                style={{ width: "100%", height: "1000px" }}
            />
            <Popup open={open} setOpen={setOpen} data={data} />
        </div>
    );
}
