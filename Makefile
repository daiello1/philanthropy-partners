TOOLS    := $(shell yarn bin)
SRC_CODE := $(shell find src/ -name "*.ts" -o -name "*.tsx" -o -name "*js")
PACKAGES  = node_modules
APP       = philanthropy-partners

.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:         Prints this screen"
	@echo "    install-deps: Installs dependencies"
	@echo "    upgrade:      Upgrades the dependencies"
	@echo "    check-fmt:    Checks the formatting of the code"
	@echo "    fmt:          Formats the code"
	@echo "    type-check:   Type checks the code"
	@echo "    lint:         Lints the code"
	@echo "    fix-lint:     Automatically applies certain lint fixes"
	@echo "    build:        Build the frontend for deployment"
	@echo "    run:          Run the frontend in a dev mode"
	@echo "    prod-build:   Build the docker image for the prod mode"
	@echo "    prod-run:     Run the prod mode"
	@echo "    clean:        Clean out temporaries"
	@echo ""

$(PACKAGES):
	@echo "Installing local dependencies"
	yarn install

.PHONY:
install-deps: $(PACKAGES)

.PHONY: upgrade
upgrade: $(PACKAGES)
	@echo "Upgrading dependencies"
	yarn upgrade-interactive --latest

.PHONY: check-fmt
check-fmt: $(PACKAGES)
	@echo "Checking the formatting of the code"
	$(TOOLS)/prettier --check $(SRC_CODE)

.PHONY: fmt
fmt: $(PACKAGES)
	@echo "Auto Formatting"
	$(TOOLS)/prettier --write $(SRC_CODE)

.PHONY: lint
lint: $(PACKAGES)
	@echo "Type Checking & Linting"
	$(TOOLS)/tsc --project tsconfig.json
	$(TOOLS)/eslint src/ $(SRC_CODE)

.PHONY: fix-lint
fix-lint: $(PACKAGES)
	@echo "Auto fixing lints"
	$(TOOLS)/eslint --fix src/

.PHONY: build
build: $(PACKAGES)
	@echo "Building the frontend"
	yarn build

.PHONY: run
run: $(PACKAGES)
	@echo "Serving site in WATCH/DEV"
	yarn run start

.PHONY: prod-build
prod-build:
	@echo "Building the production image"
	docker build . -f deployment/Dockerfile -t $(APP)

.PHONY: prod-run
prod-run: prod-build
	@echo "Serving the site from NGINX/PROD"
	docker run --rm -p 3000:80 $(APP)

.PHONY: clean
clean:
	@echo "Removing temporary files"
	rm -rf node_modules/ build/
